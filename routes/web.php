<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Event
Route::get('/','EventController@index')->name('home');

Route::get('/event/add','EventController@add_event_view')->name('add_event');
Route::post('/event/add','EventController@add_event_submit');

Route::get('/event/{id}','EventController@get_event')->name('get_event');

Route::get('/event/{id}/edit','EventController@edit_event_view')->name('edit_event');
Route::post('/event/{id}/edit','EventController@edit_event_submit');

Route::get('/event/{id}/delete','EventController@delete_event_view')->name('delete_event');
Route::post('/event/{id}/delete','EventController@delete_event_submit');

// Account
Route::get('/account/login','AccountController@login_view')->name('login');
Route::post('/account/login','AccountController@authenticate');

Route::get('/account/logout','AccountController@logout')->name('logout');