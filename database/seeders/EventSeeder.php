<?php

namespace Database\Seeders;

use DateInterval;
use DateTime;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;

class EventSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('id_ID');
        for($i=0;$i<10;$i++) {
            $event_time = $faker->dateTimeBetween('-1 days','+20 days');
            $event_time;
            $start_str = $event_time->format('Y-m-d H:i:s');
            $event_interval_hour = $faker->numberBetween(2,24);
            $event_interval_day = $faker->numberBetween(0,3);
            $event_time->add(new DateInterval('P'.$event_interval_day.'DT'.$event_interval_hour.'H'));
            $end_str = $event_time->format('Y-m-d H:i:s');
            $start = date_create_from_format('Y-m-d H:i:s',$start_str);
            $end = date_create_from_format('Y-m-d H:i:s',$end_str);
            DB::table('events')->insert([
                'name'=>$faker->text(50),
                'shortname'=>$faker->text(10),
                'start'=>$start,
                'end'=>$end,
                'description'=>$faker->text,
                'location'=>$faker->address,
                'preview'=>'sample/sample-image.jpeg'
            ]);
        }
    }
}
