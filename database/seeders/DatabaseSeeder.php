<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Database\Seeders\EventSeeder;
use Database\Seeders\UserSeeder;
use Database\Seeders\EventInvolvedSeeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // User::factory(10)->create();
        $this->call([
            EventSeeder::class,
            UserSeeder::class,
            EventInvolvedSeeder::class,
        ]);
    }
}
