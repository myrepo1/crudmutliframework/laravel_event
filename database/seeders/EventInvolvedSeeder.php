<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\Event;
use App\Models\User;

class EventInvolvedSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $event = Event::find(1);
        $user = User::where('is_staff',0)->first();
        DB::table('event_involved')->insert([
            'events_id'=>$event->id,
            'user_id'=>$user->id,
        ]);
    }
}
