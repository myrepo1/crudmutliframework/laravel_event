@foreach($fields as $field)
    <div class="form-group">
        <label for="{{$field['id']}}">{{$field['label']}}</label>
        @if($field['type']=='textarea')
        <textarea 
            name="{{$field['name']}}" 
            id="{{$field['id']}}" 
            class="{{$field['class']}} @error($field['name']) is-invalid @enderror"
            @if(isset($data))
            value="{{old($field['name'],$data[$field['name']])}}"
            @else
            value="{{old($field['name'])}}"
            @endif
        ></textarea>
        @else
        <input 
            type="{{$field['type']}}" 
            name="{{$field['name']}}" 
            id="{{$field['id']}}" 
            class="{{$field['class']}} @error($field['name']) is-invalid @enderror"
            @if(isset($data))
            value="{{old($field['name'],$data[$field['name']])}}"
            @else
            value="{{old($field['name'])}}"
            @endif
        />
        @endif
        @error($field['name'])
            <div class="invalid-feedback">{{$message}}</div>
        @enderror
    </div>
@endforeach