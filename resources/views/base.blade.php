<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Event Gallery</title>
</head>
@section('style')
<link href="{{ asset('/vendor/fontawesome-free/css/all.min.css')}}" rel="stylesheet" type="text/css">
<link href="{{ asset('/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css"/>
<link href="https://fonts.googleapis.com/css?family=Ubuntu&display=swap" rel="stylesheet"> 
<link rel="stylesheet" href="{{ asset('/css/main.css')}}"/>
@show

<body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <a class="navbar-brand" href="{{route('home')}}"><i class="fas fa-fw fa-expand"></i> Events Gallery</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
      
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav mr-auto">
            <li class="nav-item">
              <a class="nav-link" href="#">Events</a>
            </li>
            @auth
            @if(Auth::user()->is_staff)
            <li class="nav-item">
              <a class="nav-link" href="#">Accounts</a>
            </li>
            @endif
            <li class="nav-item">
              <a class="nav-link" href="#">My Accounts</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="{{route('logout')}}">Logout</a>
            </li>
            @endauth
            @guest
            <li class="nav-item">
              <a class="nav-link" href="{{route('login')}}">Login</a>
            </li>
            @endguest
          </ul>
        </div>
      </nav>
      <div class="container container-fluid">
        <div class="row">
          <h3>@yield('title')</h3>
        </div>
        <div class="row">
          @yield('content')
        </div>
      </div>

@section('script')
<script src="{{asset('/vendor/jquery/jquery-3.5.1.min.js')}}"></script>
<script src="{{asset('/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<script src="https://kit.fontawesome.com/439ccf7ee0.js"></script>
<script src="{{asset('/js/main.js')}}"></script>
@show
</body>
</html>