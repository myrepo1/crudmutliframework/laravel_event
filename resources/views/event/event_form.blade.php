@extends('base')

@section('title','Add Event')

@section('style')
@parent
@include('includes.generic_form_style')
@endsection

@section('content')
<form method="POST" novalidate enctype="multipart/form-data">
    @csrf
    @include('includes.generic_form')
    <button class="btn btn-success" type="submit">Submit</button>
</form>
@endsection

@section('script')
@parent
@include('includes.generic_form_script')
@endsection