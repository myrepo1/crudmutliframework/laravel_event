@extends('base')

@section('title',$data->name)

@section('content')

@if(Auth::user()->is_staff)
<div class="col">
    <a class="btn btn-warning" href="{{route('edit_event', $data->id)}}">Edit</a>
    <a class="btn btn-danger" href="{{route('delete_event', $data->id)}}">Delete</a>
</div>
@endif

<div class="w-100"></div>
    <div class="col">
        <div class="card">
            @if($data->preview)
            <img class="card-img-top" src="{{asset('storage/'.$data->preview)}}"/>
            @endif
            <div class="card-body">
                <h5 class="card-title">{{$data->shortname}}</h5>
                <p class="card-text">{{$data->start}}</p>
                <p class="card-text">{{$data->location}}</p>
                <p class="card-text">{{$data->description}}</p>
            </div>
            <div class="w-100"></div>
        </div>
    </div>
</div>
@endsection