@extends('base')

@section('title','Delete Event '.$data->name.'?')

@section('content')
<form method="POST" novalidate>
    @csrf
    <button class="btn btn-danger" type="submit">Yes</button>
    <a class="btn btn-success" href="{{route('get_event', $data->id)}}">Cancel</a>
</form>
@endsection