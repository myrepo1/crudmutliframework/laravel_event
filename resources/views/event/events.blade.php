@extends('base')

@section('title','Welcome to Event Gallery')

@section('content')
<div>
    <a class="btn btn-primary" href="{{route('add_event')}}">Add Event</a>
</div>
<div>
    @if ($data)
    <div class="row">
        @foreach ($data as $item)
        <div class="col col-sm-12 col-md-6 col-lg-4 mb-4">
            <div class="card">
                @if ($item->preview)
                <img class="card-img-top" src="{{asset('storage/'.$item->preview)}}"/>
                @endif
                <div class="card-body">
                    <h5 class="card-title">{{$item->shortname}}</h5>
                    <p class="card-text">{{$item->start}}</p>
                    <p class="card-text">{{$item->location}}</p>
                    <div class="row justify-content-around">
                        <a class="btn btn-primary" href="{{route('get_event',$item->id)}}">Detail</a>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
    </div>
    @endif
</div>
@endsection