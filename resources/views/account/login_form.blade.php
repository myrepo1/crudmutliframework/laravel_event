@extends('base')

@section('title','Login Form')

@section('content')
<form method="POST" novalidate enctype="multipart/form-data">
    @csrf
    @include('includes.generic_form')
    <button class="btn btn-success" type="submit">Login</button>
</form>
@endsection