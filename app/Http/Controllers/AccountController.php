<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class AccountController extends Controller
{
    public function login_view() {
        $form_fields = [
            ['field'=>'username'],
            ['field'=>'password','type'=>'password']
        ];
        $fields = $this->generate_form($form_fields);
        return view('account/login_form',['fields'=>$fields]);
    }
    
    public function authenticate(Request $request) {
        $validator = $request->validate([
            'username'=>'required',
            'password'=>'required'
        ]);

        $credentials = $request->only('username','password');

        if(Auth::attempt($credentials)) {
            return redirect()->intended();
        } else {
            $validator['username'] = 'Wrong Username or Password.';
            $validator['password'] = 'Wrong Username or Password.';
            return back()->withErrors($validator)->withInput();
        }
    }

    public function logout() {
        Auth::logout();
        return redirect('/');
    }

    public function generate_form(array $form_data) {
        $result = array();
        foreach($form_data as $input) {
            $result_input = array();
            $result_input[] = $input['field'];
            if(sizeof($input)>1) {
                if(isset($input['type'])) {
                    $result_input[] = $input['type'];
                } else {
                    $result_input[] = 'text';
                }
                if(isset($input['class'])) {
                    $result_input[] = $input['class'];
                } else {
                    $result_input[] = 'form-control';
                }
                if(isset($input['value'])) {
                    $result_input[] = $input['value'];
                }
            }
            $result[] = $this->generate_input(...$result_input);
        }
        return $result;
    }

    public function generate_input($field,$type='text',$class='form-control',$value='') {
        if($type=='file') {
            return array(
            'name'=>$field,
            'label'=>ucfirst($field),
            'id'=>$field.'Id',
            'type'=>$type,
            'class'=>$class,    
            );
        } else {
            return array(
                'name'=>$field,
                'label'=>ucfirst($field),
                'id'=>$field.'Id',
                'type'=>$type,
                'class'=>$class,
            );   
        }
    }
}
