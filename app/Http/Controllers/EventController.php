<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Models\Event;
use App\Http\Requests\EventRequest;

class EventController extends Controller
{
    public function index() {
        $data = Event::all();
        return view('event/events',['data'=>$data]);
    }

    public function add_event_view() {
        $form_fields = [
            ['field'=>'name',],
            ['field'=>'shortname'],
            ['field'=>'start','class'=>'form-control date-time-picker'],
            ['field'=>'end','class'=>'form-control date-time-picker'],
            ['field'=>'description','type'=>'textarea'],
            ['field'=>'location'],
            ['field'=>'preview','type'=>'file','class'=>'form-control-file']
        ];
        $fields = $this->generate_form($form_fields);
        return view('event/event_form',['fields'=>$fields]);
    }

    public function add_event_submit(EventRequest $request) {
        $request->validated();
        $path = null;
        if($request->file('preview')) {
            $path = $request->file('preview')->store('public/event');
            $path = str_replace('public/','',$path);
        }
        Event::create([
            'name'=>$request->name,
            'shortname'=>$request->shortname,
            'start'=>$request->start,
            'end'=>$request->end,
            'description'=>$request->description,
            'location'=>$request->location,
            'preview'=>$path
        ]);
        return redirect()->route('home');
    }

    public function get_event($id) {
        $data = Event::find($id);
        return view('event/event_detail',['data'=>$data]);
    }

    public function edit_event_view($id) {
        $record = Event::find($id);
        #Convert record to array, so it can using string
        $data = $record->toArray();
        $form_fields = [
            ['field'=>'name'],
            ['field'=>'shortname'],
            ['field'=>'start','class'=>'form-control date-time-picker'],
            ['field'=>'end','class'=>'form-control date-time-picker'],
            ['field'=>'description','type'=>'textarea'],
            ['field'=>'location'],
            ['field'=>'preview','type'=>'file','class'=>'form-control-file']
        ];
        $fields = $this->generate_form($form_fields);
        return view('event/event_form',['fields'=>$fields,'data'=>$data]);
    }

    public function edit_event_submit($id,EventRequest $request) {
        $record = Event::find($id);
        $request->validated();
        $record->name = $request->name;
        $record->shortname = $request->shortname;
        $record->start = $request->start;
        $record->end = $request->end;
        $record->description = $request->description;
        $record->location = $request->location;
        if($request->file('preview')) {
            if($record->preview&&$record->preview!='sample/sample-image.jpeg') {
                Storage::delete('public/'.$record->preview);
            }
            $path = $request->file('preview')->store('public/event');
            $path = str_replace('public/','',$path);
            $record->preview = $path;
        }
        $record->save();
        return redirect()->route('home');
    }

    public function delete_event_view($id) {
        $record = Event::find($id);
        return view('event/event_delete',['data'=>$record]);
    }

    public function delete_event_submit($id) {
        $record = Event::find($id);
        if($record->preview&&$record->preview!='sample/sample-image.jpeg') {
            Storage::delete('public/'.$record->preview);
        }
        $record->delete();
        return redirect()->route('home');
    }

    public function generate_form(array $form_data) {
        $result = array();
        foreach($form_data as $input) {
            $result_input = array();
            $result_input[] = $input['field'];
            if(sizeof($input)>1) {
                if(isset($input['type'])) {
                    $result_input[] = $input['type'];
                } else {
                    $result_input[] = 'text';
                }
                if(isset($input['class'])) {
                    $result_input[] = $input['class'];
                } else {
                    $result_input[] = 'form-control';
                }
                if(isset($input['value'])) {
                    $result_input[] = $input['value'];
                }
            }
            $result[] = $this->generate_input(...$result_input);
        }
        return $result;
    }

    public function generate_input($field,$type='text',$class='form-control',$value='') {
        if($type=='file') {
            return array(
            'name'=>$field,
            'label'=>ucfirst($field),
            'id'=>$field.'Id',
            'type'=>$type,
            'class'=>$class,    
            );
        } else {
            return array(
                'name'=>$field,
                'label'=>ucfirst($field),
                'id'=>$field.'Id',
                'type'=>$type,
                'class'=>$class,
            );   
        }
    }
}
