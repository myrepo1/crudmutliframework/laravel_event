<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EventRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $validators = [
            'start' => 'required|date_format:d-m-Y H:i',
            'end' => 'nullable||date_format:d-m-Y H:i|after:start',
            'preview'=>'image|mimes:jpeg,png,jpg,gif'
        ];
        if($this->route('id')) {
            $validators['name'] = 'required|unique:events,name,'.$this->route('id');
        } else {
            $validators['name'] = 'required|unique:events,name';
        }
        return $validators;
    }
}
